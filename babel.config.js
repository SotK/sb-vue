module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins: [
    [
      'prismjs',
      {
        languages: ['javascript', 'css', 'html'],
        plugins: ['line-numbers', 'show-language'],
        theme: 'tomorrow',
        css: true
      }
    ]
  ]
}
