<!--
  ~ Copyright (c) 2020 Adam Coldrick, Jack Coldrick
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License"); you may
  ~ not use this file except in compliance with the License. You may obtain
  ~ a copy of the License at
  ~
  ~         http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
  ~ WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
  ~ License for the specific language governing permissions and limitations
  ~ under the License.
  -->

<template>
  <div class="story-filter-bar">
    <ChipInputDropdown
      :options="options"
      :chips="chips"
      :keepFocus="true"
      @select="optionSelected"
      @input="search"
      @delete="deleteFilter"
      @keyup.esc="resetSearch"
      v-model="searchTerm"
    >
      <template v-slot:left>
        <div class="filtering-container">
          <p>Filtering:</p>
        </div>
      </template>
    </ChipInputDropdown>
  </div>
</template>

<script>
import user from '@/api/user.js'
import tag from '@/api/tag.js'
import project from '@/api/project.js'
import projectGroup from '@/api/project_group.js'
import ChipInputDropdown from './ChipInputDropdown'

const SEARCH_OPTIONS = {
  STATUS: 'status',
  ASSIGNEE: 'assignee_id',
  CREATOR: 'creator_id',
  PROJECT_GROUP: 'project_group_id',
  PROJECT: 'project_id',
  TAGS: 'tags'
}

export default {
  name: 'StoryFilters',
  components: {
    ChipInputDropdown
  },
  data () {
    return {
      searchFilter: {},
      searchTerm: '',
      currentSearchResults: [],
      filters: [
        {
          key: SEARCH_OPTIONS.STATUS,
          name: 'Status',
          value: null,
          active: false
        },
        {
          key: SEARCH_OPTIONS.ASSIGNEE,
          name: 'Assignee',
          value: null,
          active: false
        },
        {
          key: SEARCH_OPTIONS.CREATOR,
          name: 'Creator',
          value: null,
          active: false
        },
        {
          key: SEARCH_OPTIONS.PROJECT_GROUP,
          name: 'Project Group',
          value: null,
          active: false
        },
        {
          key: SEARCH_OPTIONS.PROJECT,
          name: 'Project',
          value: null,
          active: false
        },
        {
          key: SEARCH_OPTIONS.TAGS,
          name: 'Tags',
          value: null,
          active: false
        }
      ]
    }
  },
  computed: {
    options () {
      if (this.searchFilter.name && !this.currentSearchResults.length) {
        return []
      }

      if (this.currentSearchResults.length) {
        return this.currentSearchResults
      }

      return this.inactiveFilters.map(filter => ({ id: filter.key, name: filter.name }))
    },
    chips () {
      const activeFilters = this.activeFilters.map(filter => ({ id: filter.key, name: `${filter.name}: ${filter.value.name}` }))
      if (this.searchFilter.name) {
        activeFilters.push({ id: this.searchFilter.key, name: this.searchFilter.name })
      }

      return activeFilters
    },
    activeFilters () {
      return this.filters.filter(filter => filter.active)
    },
    inactiveFilters () {
      return this.filters.filter(filter => !filter.active)
    },
    formattedFilters () {
      return this.activeFilters.reduce((acc, filter) => {
        acc[filter.key] = filter.value.id
        return acc
      }, {})
    }
  },
  created () {
    this.filtersFromQuery()
  },
  methods: {
    optionSelected (option) {
      this.searchTerm = ''
      const selectedFilter = this.filters.find(filter => filter.key === option.id)
      if (selectedFilter) {
        this.searchFilter = selectedFilter
        this.search()
      } else {
        this.addSearchValue(option)
      }
    },

    async search () {
      if (!this.searchFilter.key) {
        return
      }

      let results

      switch (this.searchFilter.key) {
        case SEARCH_OPTIONS.STATUS: {
          results = ['Active', 'Merged', 'Invalid'].map(result => ({ id: result.toLowerCase(), name: result }))
          break
        }

        case SEARCH_OPTIONS.ASSIGNEE:
        case SEARCH_OPTIONS.CREATOR: {
          const params = {
            full_name: this.searchTerm,
            limit: 5
          }

          const users = await user.browse(params)
          results = users.map(user => ({ id: user.id, name: user.full_name }))
          break
        }

        case SEARCH_OPTIONS.TAGS: {
          const params = {
            name: this.searchTerm,
            limit: 5
          }

          const tags = await tag.browse(params)
          results = tags.map(result => ({ id: result.name, name: result.name }))
          break
        }

        case SEARCH_OPTIONS.PROJECT_GROUP: {
          const params = {
            name: this.searchTerm,
            limit: 5
          }

          const groups = await projectGroup.browse(params)
          results = groups.map(result => ({ id: result.id, name: result.name }))
          break
        }

        case SEARCH_OPTIONS.PROJECT: {
          const params = {
            name: this.searchTerm,
            limit: 5
          }

          const projects = await project.browse(params)
          results = projects.map(result => ({ id: result.id, name: result.name }))
          break
        }
      }

      this.currentSearchResults = results.filter(result => result.name !== null)
    },

    addSearchValue (value) {
      const filterIndex = this.filters.findIndex(filter => filter.key === this.searchFilter.key)
      this.filters[filterIndex].value = value
      this.filters[filterIndex].active = true

      this.searchFilter = {}
      this.currentSearchResults = []
      this.searchTerm = ''
      this.$emit('filter-change', this.formattedFilters)
    },

    deleteFilter (filter) {
      const filterIndex = this.filters.findIndex(f => f.key === filter.id)
      if (!this.filters[filterIndex].active) {
        this.resetSearch()
      }

      this.filters[filterIndex].active = false
      this.$emit('filter-change', this.formattedFilters)
    },

    resetSearch () {
      this.searchFilter = {}
      this.currentSearchResults = []
    },

    async filtersFromQuery () {
      Object.entries(this.$route.query).forEach(async param => {
        const [key, value] = param
        const filterIndex = this.filters.findIndex(filter => filter.key === key)

        switch (key) {
          case SEARCH_OPTIONS.STATUS: {
            this.filters[filterIndex].value = {
              id: value,
              name: value[0].toUpperCase() + value.slice(1)
            }
            this.filters[filterIndex].active = true
            break
          }

          case SEARCH_OPTIONS.ASSIGNEE:
          case SEARCH_OPTIONS.CREATOR: {
            const filterUser = await user.get(value)
            this.filters[filterIndex].value = {
              id: value,
              name: filterUser.full_name
            }
            this.filters[filterIndex].active = true
            break
          }

          case SEARCH_OPTIONS.TAGS: {
            this.filters[filterIndex].value = {
              id: value,
              name: value
            }
            this.filters[filterIndex].active = true
            break
          }

          case SEARCH_OPTIONS.PROJECT_GROUP: {
            const filterProjectGroup = await projectGroup.get(value)
            this.filters[filterIndex].value = {
              id: value,
              name: filterProjectGroup.title
            }
            this.filters[filterIndex].active = true
            break
          }

          case SEARCH_OPTIONS.PROJECT: {
            const filterProject = await project.get(value)
            this.filters[filterIndex].value = {
              id: value,
              name: filterProject.name
            }
            this.filters[filterIndex].active = true
            break
          }
        }
      })
    }
  }
}
</script>

<style lang="scss" scoped>
.story-filter-bar {
  margin-bottom: 30px;
}

.filtering-container {
  display: flex;
}
</style>
