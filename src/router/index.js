/*
 * Copyright (c) 2020 Adam Coldrick
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/story',
    name: 'Stories',
    component: () => import(/* webpackChunkName: "story" */ '../views/StoryList.vue')
  },
  {
    path: '/story/:id',
    component: () => import(/* webpackChunkName: "story-detail" */ '../views/StoryDetail.vue')
  },
  {
    path: '/board',
    name: 'Boards',
    component: () => import(/* webpackChunkName: "board" */ '../views/BoardList.vue')
  },
  {
    path: '/worklist',
    name: 'Worklists',
    component: () => import(/* webpackChunkName: "worklist" */ '../views/WorklistList.vue')
  },
  {
    path: '/project',
    name: 'Projects',
    component: () => import(/* webpackChunkName: "project" */ '../views/ProjectList.vue')
  },
  {
    path: '/project/:id',
    component: () => import(/* webpackChunkName: "project-detail" */ '../views/ProjectDetail.vue')
  },
  {
    path: '/project-group',
    name: 'Project Groups',
    component: () => import(/* webpackChunkName: "projectgroup" */ '../views/ProjectGroupList.vue')
  },
  {
    path: '/project-group/:id',
    component: () => import(/* webpackChunkName: "projectgroup-detail" */ '../views/ProjectGroupDetail.vue')
  },
  {
    path: '/user',
    name: 'Users',
    component: () => import(/* webpackChunkName: "user" */ '../views/UserList.vue')
  },
  {
    path: '/user/:id',
    component: () => import(/* webpackChunkName: "user-detail" */ '../views/UserDetail.vue')
  },
  {
    path: '/team',
    name: 'Teams',
    component: () => import(/* webpackChunkName: "team" */ '../views/TeamList.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
